﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Web.Interfaces;
using Web.Model;

namespace Web.Services
{
  public class CustomerService : ICustomerService
  {
    private Context Context { get; }
    private IAccountService AccountService { get; }

    public CustomerService(Context context, IAccountService accountService)
    {
      Context = context;
      AccountService = accountService;
    }

    public async Task<bool> IsCustomerValid(Guid customerId)
    {
      Customer customer = await Context.Customers
        .SingleOrDefaultAsync(a => a.Id == customerId);

      if (customer == null)
        return false;

      DateTime now = DateTime.Now;
      if (now < customer.RegisteredOn || now > customer.DeactivatedOn)
        return false;

      return true;
    }

    // todo Implement method for retrieving information about the customer using AccountService.

    public async Task RaiseAlert(Guid customerId)
    {
      // todo Invoke appropriate workflow.
    }
  }
}