﻿using System;

namespace Web.Model
{
  public class Transaction
  {
    public Guid Id { get; set; }
    public Guid AccountId { get; set; }
    public double Amount { get; set; }
    public DateTime Occasion { get; set; }

    public virtual Account Account { get; set; }
  }
}
