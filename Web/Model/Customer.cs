﻿using System;
using System.Collections.Generic;

namespace Web.Model
{
  public class Customer
  {
    public Guid Id { get; set; }
    public Name Name { get; set; }
    public Address Address { get; set; }
    public DateTime RegisteredOn { get; set; }
    public DateTime DeactivatedOn { get; set; }

    public virtual IEnumerable<Account> Accounts { get; set; }
    public string FullName => $"{Name.Prefix}{Name.First} {Name.Last}";
  }
}