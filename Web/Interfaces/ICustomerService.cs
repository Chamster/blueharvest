﻿using System;
using System.Threading.Tasks;

namespace Web.Interfaces
{
  public interface ICustomerService
  {
    Task<bool> IsCustomerValid(Guid customerId);
    Task RaiseAlert(Guid customerId);
  }
}