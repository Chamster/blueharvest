﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.Interfaces;
using Web.Model;

namespace Web.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class AccountController : BaseController
  {
    private IAccountService Service { get; }

    public AccountController(IAccountService service, Context context) : base(context)
    {
      Service = service;
    }

    [HttpPost("{customerId}/{amount}")]
    public ActionResult<Guid> CreateAccountByCustomer(Guid customerId, double amount)
    {
      Task<Guid> guid = Service.CreateByCustomerAsync(customerId, amount);

      if (guid.IsCompletedSuccessfully)
        return Ok(guid.Result);
      return NotFound(customerId);
    }

    #region Defaults

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Account>>> GetAccounts()
    {
      return await Context.Accounts.ToListAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Account>> GetAccount(Guid id)
    {
      Account account = await Context.Accounts.FindAsync(id);

      if (account == null)
        return NotFound();

      return account;
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> PutAccount(Guid id, Account account)
    {
      if (id != account.Id)
        return BadRequest();

      Context.Entry(account).State = EntityState.Modified;

      try { await Context.SaveChangesAsync(); }
      catch (DbUpdateConcurrencyException)
      {
        if (AccountExists(id))
          throw;
        return NotFound();
      }

      return NoContent();
    }

    [HttpPost]
    public async Task<ActionResult<Account>> PostAccount(Account account)
    {
      Context.Accounts.Add(account);
      await Context.SaveChangesAsync();

      return CreatedAtAction("GetAccount", new { id = account.Id }, account);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<Account>> DeleteAccount(Guid id)
    {
      Account account = await Context.Accounts.FindAsync(id);
      if (account == null)
        return NotFound();

      Context.Accounts.Remove(account);
      await Context.SaveChangesAsync();

      return account;
    }

    #endregion

    private bool AccountExists(Guid id)
    {
      return Context.Accounts.Any(e => e.Id == id);
    }
  }
}