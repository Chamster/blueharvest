﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Web.Interfaces;
using Web.Model;

namespace Web.Services
{
  public class TransactionService : ITransactionService
  {
    private Context Context { get; }
    private IAccountService AccountService { get; }

    public TransactionService(Context context, IAccountService accountService)
    {
      Context = context;
      AccountService = accountService;
    }

    public async Task<Guid> CreateByAccountAndAmountAsync(Guid accountId, double amount)
    {
      Transaction transaction = new Transaction { AccountId = accountId, Amount = amount, Occasion = DateTime.Now };
      await Context.Transactions.AddAsync(transaction);

      double sum = Context.Transactions
        .Where(a => a.AccountId == accountId)
        .Sum(a => a.Amount);
      if (sum < 0)
        AccountService.RaiseAlert(accountId);

      return transaction.Id;
    }
  }
}