﻿namespace Web.Model
{
  public class Name
  {
    public string Prefix { get; set; }
    public string First { get; set; }
    public string Last { get; set; }
    public string Nick { get; set; }
  }
}