﻿using System;
using System.Collections.Generic;

namespace Web.Model
{
  public class Account
  {
    public Guid Id { get; set; }
    public Guid CustomerId { get; set; }
    public double Balance { get; set; }

    public virtual Customer Customer { get; set; }
    public virtual IEnumerable<Transaction> Transactions { get; set; }
  }
}