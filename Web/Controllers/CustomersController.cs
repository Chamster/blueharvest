﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.Model;

namespace Web.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class CustomersController : BaseController
  {
    #region Defaults

    public CustomersController(Context context) : base(context) { }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Customer>>> GetCustomers()
    {
      return await Context.Customers.ToListAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Customer>> GetCustomer(Guid id)
    {
      Customer customer = await Context.Customers.FindAsync(id);

      if (customer == null)
        return NotFound();

      return customer;
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> PutCustomer(Guid id, Customer customer)
    {
      if (id != customer.Id)
        return BadRequest();

      Context.Entry(customer).State = EntityState.Modified;

      try { await Context.SaveChangesAsync(); }
      catch (DbUpdateConcurrencyException)
      {
        if (CustomerExists(id))
          throw;
        return NotFound();
      }

      return NoContent();
    }

    [HttpPost]
    public async Task<ActionResult<Customer>> PostCustomer(Customer customer)
    {
      Context.Customers.Add(customer);
      await Context.SaveChangesAsync();

      return CreatedAtAction("GetCustomer", new { id = customer.Id }, customer);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<Customer>> DeleteCustomer(Guid id)
    {
      Customer customer = await Context.Customers.FindAsync(id);
      if (customer == null)
        return NotFound();

      Context.Customers.Remove(customer);
      await Context.SaveChangesAsync();

      return customer;
    }

    #endregion

    private bool CustomerExists(Guid id)
    {
      return Context.Customers.Any(e => e.Id == id);
    }
  }
}