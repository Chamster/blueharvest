﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.Model;

namespace Web.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class TransactionsController : BaseController
  {
    #region Defaults

    public TransactionsController(Context context) : base(context) { }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Transaction>>> GetTransactions()
    {
      return await Context.Transactions.ToListAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Transaction>> GetTransaction(Guid id)
    {
      Transaction transaction = await Context.Transactions.FindAsync(id);

      if (transaction == null)
        return NotFound();

      return transaction;
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> PutTransaction(Guid id, Transaction transaction)
    {
      if (id != transaction.Id)
        return BadRequest();

      Context.Entry(transaction).State = EntityState.Modified;

      try { await Context.SaveChangesAsync(); }
      catch (DbUpdateConcurrencyException)
      {
        if (TransactionExists(id))
          throw;
        return NotFound();
      }

      return NoContent();
    }

    [HttpPost]
    public async Task<ActionResult<Transaction>> PostTransaction(Transaction transaction)
    {
      Context.Transactions.Add(transaction);
      await Context.SaveChangesAsync();

      return CreatedAtAction("GetTransaction", new { id = transaction.Id }, transaction);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<Transaction>> DeleteTransaction(Guid id)
    {
      Transaction transaction = await Context.Transactions.FindAsync(id);
      if (transaction == null)
        return NotFound();

      Context.Transactions.Remove(transaction);
      await Context.SaveChangesAsync();

      return transaction;
    }

    #endregion

    private bool TransactionExists(Guid id)
    {
      return Context.Transactions.Any(e => e.Id == id);
    }
  }
}
