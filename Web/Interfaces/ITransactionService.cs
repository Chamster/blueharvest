﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.Interfaces
{
  public interface ITransactionService
  {
    Task<Guid> CreateByAccountAndAmountAsync(Guid accountId, double amount);
  }
}