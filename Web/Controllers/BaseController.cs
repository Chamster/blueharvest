﻿using Microsoft.AspNetCore.Mvc;
using Web.Model;

namespace Web.Controllers
{
  public class BaseController : ControllerBase
  {
    protected Context Context { get; }

    public BaseController(Context context)
    {
      Context = context;
    }
  }
}