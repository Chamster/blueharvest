using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Web.Interfaces;
using Web.Model;
using Web.Services;

namespace Web
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Info { Title = "Blue Harvest Bank API", Version = "v1" }));

      services.AddDbContext<Context>(opt => opt.UseInMemoryDatabase("BlueHarvestBank"));

      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

      services.AddSpaStaticFiles(configuration => configuration.RootPath = "ClientApp/dist");

      services.AddScoped<ICustomerService, CustomerService>();
      services.AddScoped<IAccountService, AccountService>();
      services.AddScoped<ITransactionService, TransactionService>();
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
        app.UseDeveloperExceptionPage();
      else
      {
        app.UseExceptionHandler("/Error");
        app.UseHsts();
      }

      app.UseHttpsRedirection();
      app.UseStaticFiles();
      app.UseSpaStaticFiles();

      app.UseSwagger();
      app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BHB API v1"));

      app.UseMvc(routes => routes.MapRoute("default", "{controller}/{action=Index}/{id?}"));

      app.UseSpa(spa =>
      {
        spa.Options.SourcePath = "ClientApp";
        if (env.IsDevelopment())
          spa.UseAngularCliServer("start");
      });
    }
  }
}
