﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Web.Interfaces;
using Web.Model;

namespace Web.Services
{
  public class AccountService : IAccountService
  {
    private Context Context { get; }
    private ICustomerService CustomerService { get; }
    private ITransactionService TransactionService { get; }

    public AccountService(Context context, ICustomerService customerService, ITransactionService transactionService)
    {
      Context = context;
      CustomerService = customerService;
      TransactionService = transactionService;
    }

    public async Task<Guid> CreateByCustomerAsync(Guid customerId, double amount = 0)
    {
      Customer customer = Context.Customers
        .SingleOrDefault(a => a.Id == customerId);
      if (customer == null)
        throw new Exception($"Invalid customer ID ({customerId})!");

      bool valid = await CustomerService.IsCustomerValid(customerId);
      if (!valid)
        throw new Exception($"Inactive customer ({customer.RegisteredOn:yyyy-MM-dd} - {customer.DeactivatedOn:yyyy-MM-dd})!");

      Account account = new Account { CustomerId = customerId };
      Context.Accounts.Add(account);
      await Context.SaveChangesAsync();

      if (amount != 0)
        TransactionService.CreateByAccountAndAmountAsync(account.Id, amount);

      return account.Id;
    }

    public async Task RaiseAlert(Guid accountId)
    {
      Account customer = Context.Accounts
        .Include(a => a.Customer)
        .Single(a => a.Id == accountId);

      CustomerService.RaiseAlert(customer.Id);
    }
  }
}
