﻿using System;
using System.Threading.Tasks;
using Web.Model;

namespace Web.Interfaces
{
  public interface IAccountService
  {
    Task<Guid> CreateByCustomerAsync(Guid customerId, double amount = 0);
    Task RaiseAlert(Guid accountId);
  }
}
