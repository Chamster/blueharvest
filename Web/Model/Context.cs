﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Query.Expressions;

namespace Web.Model
{
  public class Context : DbContext
  {
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Account> Accounts { get; set; }
    public DbSet<Transaction> Transactions { get; set; }

    public Context(DbContextOptions options) : base(options) { }

    public void Seed()
    {
      Customers.AddRange(SeedCustomers());
      Accounts.AddRange(SeedAccounts());
      Transactions.AddRange(SeedTransactions());
      SaveChangesAsync();
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);

      OnModelCreating(builder.Entity<Customer>());
      OnModelCreating(builder.Entity<Account>());
      OnModelCreating(builder.Entity<Transaction>());
    }

    private static void OnModelCreating(EntityTypeBuilder<Customer> entity)
    {
      entity.OwnsOne(a => a.Name);
      entity.OwnsOne(a => a.Address);
    }

    private static void OnModelCreating(EntityTypeBuilder<Account> entity)
    {
      entity.HasOne(a => a.Customer).WithMany(a => a.Accounts);
    }

    private static void OnModelCreating(EntityTypeBuilder<Transaction> entity)
    {
      entity.HasOne(a => a.Account).WithMany(a => a.Transactions);
    }

    private IEnumerable<Customer> SeedCustomers()
    {
      DateTime now = DateTime.Now;

      yield return new Customer
      {
        Address = new Address { City = "Stockholm", State = "Sweden", Street = "Donkey Street 1" },
        Name = new Name { First = "Konrad", Last = "Viltersten" },
        RegisteredOn = now.AddDays(-1),
        DeactivatedOn = now.AddDays(360),
        Id = new Guid("12345678-1234-b00b-1234-123456789aaa")
      };

      yield return new Customer
      {
        Address = new Address { City = "Gothenburg", State = "Sweden", Street = "King's Square 7" },
        Name = new Name { First = "Future", Last = "Customer" },
        RegisteredOn = now.AddDays(1),
        DeactivatedOn = now.AddDays(360),
        Id = new Guid("12345678-1234-b00b-1234-123456789aab")
      };

      yield return new Customer
      {
        Address = new Address { City = "Helsinki", State = "Finland", Street = "Hirvi Paska 13" },
        Name = new Name { First = "Deactivated", Last = "Smuck" },
        RegisteredOn = now.AddDays(-100),
        DeactivatedOn = now.AddDays(-10),
        Id = new Guid("12345678-1234-b00b-1234-123456789aac")
      };
    }

    private IEnumerable<Account> SeedAccounts()
    {
      yield break;
    }

    private IEnumerable<Transaction> SeedTransactions()
    {
      yield break;
    }
  }
}